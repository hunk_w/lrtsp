#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include "lrtsp_server.h"
#include "lrtsp_stream.h"


#define TEST_H264_FILE  "li.h264"

#define OUT_BUFFER_LEN 1024*1024

typedef struct _HdyFrameHeader
{   
    char version[8];                /*固定的版本号*/    
    unsigned char channel;          /*通道号 【0-15】通道号1 对应0*/
    unsigned char encoder_format;   /*编码类型 0-- h.264 1---jpeg 2--mjpeg, 3--mpeg4, 4--g711a, 5--g711u, 6---g726, 7---adpcma, 8---lpcm*/
    unsigned char frame_type;       /*帧类型  0---I帧， 1---P帧, 2---B帧, 3---audio帧*/
    unsigned char fps;              /*帧率*/
    unsigned int frame_length;      /*帧数据的长度*/
    unsigned int frame_seq;         /*帧序号*/
    unsigned long long pts;         /*时间戳*/
    unsigned int reserve;           
}HdyFrameHeader;

#define FRAME_HEADER_VERSION    "DVE1601"

int _pull_stream_data(struct LrtspStream* thiz, void** stream_data, int* len, struct LRSDataHeader* header)
{   
    static int is_first = 1;
    FILE* fp = NULL;
	struct stat st = {0};
	static unsigned char* data = NULL;
	static unsigned char* p = NULL;
	static int read_pos = 0;
	static int file_len = 0;
	unsigned char out_buffer[OUT_BUFFER_LEN] = {0};
    HdyFrameHeader* freme_header = NULL;
    int ret = -1;
    struct timeval now = {0};

    if(is_first){
        if(stat(TEST_H264_FILE, &st) != 0){
            perror("stat failed!\n");
            exit(-1);
        }
        file_len  = st.st_size;
        /*read the file data to buffer*/
        fp = fopen(TEST_H264_FILE, "rb");
    	if(fp != NULL){
		    data = malloc(st.st_size + 1);
		    ret = fread(data, 1, st.st_size, fp);
		    if(ret != st.st_size )
		    {
                perror("fread failed!\n");
		    }
		    fclose(fp);
	    }
    	else{
            perror("open settings file failed!\n");
            exit(-1);
    	}
    	p = data;
    	is_first = 0;
    }

    if(read_pos >= file_len){
        read_pos = 0;
        p = data;
    }       

    while(read_pos < file_len){
        if(strcmp((char*)p, FRAME_HEADER_VERSION) == 0){
            freme_header = (HdyFrameHeader*)p;
            p += sizeof(HdyFrameHeader);
            read_pos += sizeof(HdyFrameHeader);
//            printf("func(%s), line(%d): freme_header->frame_length(%d)  fps(%d) pts(%lld) sizeof header(%d)\n", __func__, __LINE__, freme_header->frame_length, freme_header->fps, freme_header->pts,sizeof(HdyFrameHeader));
            memcpy(out_buffer, p, freme_header->frame_length);
            
            *stream_data = out_buffer;
            *len = freme_header->frame_length;
            gettimeofday(&now, NULL);
            header->pts = (now.tv_sec*1000 + now.tv_usec/1000)*90;
            p += freme_header->frame_length;
            read_pos += freme_header->frame_length;
            ret = 0;
            usleep(25*1000);
            break;
        }
        else{
           ++p;
           ++read_pos;
        }
    }

    return ret;
}

int main(int argc, char* argv[])
{
    struct LrtspStream* stream = NULL;
	lrtsp_server_init("192.168.1.129", "1222");

    stream = (struct LrtspStream*)malloc(sizeof(struct LrtspStream));
    strcpy(stream->rtsp_filename, "1main");
    stream->pull_stream_data = _pull_stream_data;

    lrtsp_stream_manager_add_stream(stream);

	lrtsp_server_run();

	lrtsp_server_deinit();

	return 0;
}

