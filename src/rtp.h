#ifndef RTP_H
#define RTP_H
#include  "rtsp_client.h"
#include "lrtsp_stream.h"



int rtp_send_data(struct RtspClient* rtsp_client,
                                unsigned char* data, 
                                int data_len, 
                                struct LRSDataHeader header);
#endif /*RTP_H*/
